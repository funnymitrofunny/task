import svg4everybody from 'svg4everybody';
import $ from 'jquery';
import '../blocks/range/range.js';
import '../blocks/custom-textarea/custom-textarea.js';
import '../blocks/info/info.js';
import '../blocks/date/date.js';

$(() => {
	svg4everybody();
});
