import $ from 'jquery';

function setCurrentDate() {
	const n = new Date();
	const d = n.getDate();
	let m = n.getMonth() + 1;
	const y = n.getFullYear();
	if (m < 10){
		m = '0' + m;
	}
	$('#date').val(d + '.' + m + '.' + y);
}

setCurrentDate();
