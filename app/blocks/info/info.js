import $ from 'jquery';

// Allow only digits in "Year" input field
function validateYear() {
	const inputField = $('.info__entire-width-input');
	inputField.each(function () {
		if ($(this).attr('name') === 'birth') {
			$(this).keypress(function (evt) {
				if ((evt.which < 48) || (evt.which > 57)) {
					return false;
				}
			});
		}
	});
}

validateYear();
