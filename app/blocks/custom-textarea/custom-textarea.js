import $ from 'jquery';

const ta = $('.custom-textarea');

// Set textarea size according to the length of text
function setInitialTextareaHeight() {
	const initHeight = ta.prop('scrollHeight');
	ta.attr('style', 'height:' + initHeight + 'px');
}

// Add strings to textarea when available space has ended
function addTextareaStrings() {
	const initHeight = ta.height() - ta.css('padding-top').replace(/[^-\d\.]/g, ''); // textarea height without top padding
	$('.about').on('focus input keyup keydown', '.custom-textarea', function (){
		$(this).height(initHeight).height(this.scrollHeight);
	}).find('.custom-textarea').change();
}

setInitialTextareaHeight();
addTextareaStrings();
