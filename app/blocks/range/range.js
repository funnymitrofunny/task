import $ from 'jquery';

const range = $('.range__inner');
const thumb = $('.range__thumb');

// Change thumb position
function moveRange(evt) {
	const thumbCenterX = thumb.width() / 2;
	const thumbPosX = evt.pageX - range.offset().left;
	const thumbPosY = evt.pageY - range.offset().top;
	if ((thumbPosX >= 0) && (thumbPosX <= range.width() - thumbCenterX) && (thumbPosY >= 0) && (thumbPosY <= range.height())) {
		thumb.css('left', thumbPosX + 'px');
	}
}

// Set lables automatically taking them from attribute "labels"
function setRangeLabels() {
	let labels = range.attr('labels');
	labels = labels.split(', ');
	range.append('<div class="range__labels"></div>');
	$(labels).each(function (index, value) {
		range.find('.range__labels').append('<div class="range__label">' + value + '</div>');
	});
}

range.mousedown(function () {
	$(document).mousemove(moveRange);
});

range.click(function () {
	$(document).click(moveRange);
});

$(document).mouseup(function () {
	$(document).off('mousemove');
});

setRangeLabels();
